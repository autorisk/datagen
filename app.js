
//module for schema validation
const Joi = require('joi');
//middleware to To parse the data in the body
const bodyParser = require('body-parser');
//loading the path module
const path = require('path');
//loading the OS module
const os = require('os');
//loading the filesystem module
const fs = require('fs');

//-----MongoDB connection-----
var mongodb = require('mongodb');
var mongoose = require("mongoose");
const mongojs = require('mongojs');
//const db = mongojs(ConnecgtionString,[collections]);
const db = mongojs('Risks', ['risks']);

const express = require('express');
const app = express();

//enable parsing a JSON objects from the request
app.use(express.json());


//-----Store all JS and CSS in public folder.-----
//app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static(path.join(__dirname, 'public')))
//-----Store all HTML files in views folder.-----
//app.set("view options", { layout: false });
//app.use(express.static(__dirname + '/views'));

//module to generate data
const faker = require('faker');

//---------------create a list of risks--------------------
var listOfRisks = new Array();//[];
var tot_elements = 0;
var name = new Array();
name = ['Insufficient investment in training',
    'Over-reliance on one or a few key employees',
    'Inability to recruit / retain staff',
    'Failure to develop appropriate skills',
    'Loss of key employees',
    'Not offering attractive (enough) remuneration packages',
    'Ineffective health and safety management',
    'Lack of effective succession planning',
    'New or inexperienced management',
    'Not offering attractive career paths',
    'Employee / director fraud',
    'Lack of training',
    'Unexpected / unbudgeted cost increases',
    'Failure to achieve margins',
    'Failure to manage debt to equity ratio',
    'Ineffective asset management',
    'Ineffective assets performance measurement',
    'Accounting adjustments',
    'Insufficient capital availability',
    'Inadequate systems to calculate tand monitor margins',
    'Failure to collect all income due',
    'Adverse impact of exchange rate fluctuations',
    'Failure to meet earnings targets',
    'Ineffective buying / spending on or using of financing methods',
    'Alternative funding sources identified are not appropriate',
    'Failure to achieve profitability goals',
    'Operating Margin Risks',
    'Assets not properly secured and insured',
    'Strategy is not implemented',
    'Failure to have and execute R&D plans',
    'Risk management practices do not exist',
    'Regulatory / legal limitations',
    'No change management process',
    'Systems are not integrated',
    'Strategy is implemented improperly',
    'Brand / reputation damage by company environmental failure',
    'Failure to identify market opportunities',
    'Increase of competitors in the marketplace',
    'Falling market conditions',
    'Criminal act causing environmental damage',
    'Environmentally hazardous / sensitive materials not managed effectively',
    'Increased raw materials prices due to market shortages caused by environmental incident',
    'Ineffective recycling strategies',
    'Competitive advantage period is not known',
    'Supply of raw materials affected by environmental failure',
    'Aggressive competitor activity',
    'Uncompetitive products and services',
    'Lack information regarding sales prices and profit margins',
    'Failure to meet demand',
    'Loss of business through damage to reputation of company',
    'New opportunities to create or extend competitive advantage periods are not considered',
    'Sales personnel unaware of the period of advantage and do not know their products and services',
    'Loss of business through unattractive product / service offerings',
    'Loss to competitors through competitor efforts',
    'Competitive Advantage Period Risks',
    'Poor value for money',
    'Diversification of products and services at the expense of core business',
    'Aggressive competitor activity',
    'Natural disaster',
    'Changing customer expectations',
    'Failure to achieve global expansion',
    'Product development not matching external needs',
    'Regulatory / legal constraints',
    'Risk in volatility in emerging markets',
    'Poor quality / reliability record',
    'Competitive period is not measured and monitored',
    'Adverse market conditions',
    'Community involvement objectives are not clearly articulated',
    'Imposition of import/export taxes in home or overseas market',
    'Foreign government support for home market',
    'Production shutdown by regulators for law / regulation infringement',
    'Government shuts down overseas / export market for political reasons',
    'Charitable contributions by foreign operations are not well controlled',
    'Implication of new labor laws',
    'Failure to recognize community awareness in foreign countries resulting in adverse publicity',
    'Ineffective public relations causing adverse publicity',
    'Time off for community involvement is no adequately controlled',
    'Industrial sabotage',
    'Legal changes affecting the availability of raw materials',
    'Failure to comply with government ethical policies / regulations',
    'Community awareness programs are ineffective',
    'Failure to comply with government sponsored ethical policies / regulations',
    'Charitable contributions are not adequately controlled',
    'Increased tax burden on products / profits',
    'Fines for failure to comply with laws / regulations',
    'Customer needs change and organization is not aware',
    'Changing customer expectations',
    'High service costs will reduce profitability over the product life cycle',
    'Loss of customers through poor customer after sales service',
    'Customer service is not sufficient',
    'Customer fraud',
    'Customer is not satisfied',
    'Failure to focus on and manage key customers effectively',
    'Failure to optimize customer access to the company by electronic means (E-commerce)',
    'Loss of customers through poor customer management',
    'Unauthorized changes will be made to programs, master files, and reference data',
    'Data will not be processed accurately or properly',
    'Systems will not be properly implemented',
    'Systems will not be designed according to user needs',
    'Fraud'];

function fill_values(total, inh_rr, res_arr, origin_arr, excpected_arr) {
    for (var i = 0; i < total; i++) {
        var res_score_data = faker.helpers.randomize(res_arr);
        var risk = {
            audit_is: faker.random.number(1, 100),
            name: faker.random.arrayElement(name),
            risk_id: tot_elements + 1,//faker.random.uuid(),
            date_time: faker.date.past(2),
            inherent_score: faker.helpers.randomize(inh_rr),
            residual_score: res_score_data, //.random.number(3,5),
            department: faker.commerce.department(),
            location_id: faker.address.city(),
            grup_team: faker.commerce.color(),
            control_count: faker.random.number(1, 75),
            businness_contact: faker.name.findName(),
            user_entered: faker.name.findName(),
            issue_count: faker.random.number(1, 10),
            risk_description: faker.lorem.sentence(5),
            org_id: faker.random.uuid(),
            biz_container: faker.random.number(10, 100),
            origin_score: faker.helpers.randomize(origin_arr),
            expected_result: faker.helpers.randomize(excpected_arr),
            proc_count: res_score_data * 2,
            //department_id:faker.random.number(1,500),
        };
        listOfRisks.push(risk);
        tot_elements++;
    }
    listOfRisks.sort(function (a, b) { return a.risk_id - b.risk_id });
}

fill_values(50, [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2]);
fill_values(50, [4, 5], [4, 5], [4, 5], [4, 5]);
fill_values(900, [2, 3, 4, 5], [2, 3, 4, 5], [2, 3, 4, 5], [2, 3, 4, 5]);
fill_values(1, [20, 20], [20, 20], [20, 20], [20, 20]);

//console.log(listOfRisks);
//--------write in a file-----------
fs.writeFile("./public/file_data/data.txt", JSON.stringify(listOfRisks, null, 3), 'utf8', (err) => {
    if (err) { console.log(err); return; }
    else console.log('File saved')
})


//----------Insert into mongodb---------------------------
//using mlab
/*const MongoClient = require('mongodb').Mon,goClient;
 
const mongo_url = 'mongodb://admin:admin@ds263989.mlab.com:63989/autorisk';
 
MongoClient.connect(mongo_url, (err, db) => {
    if (err) throw err;
    console.log('Success');
    /*var dbo = db.db("autorisk");
    //var query = { address: "Park Lane 38" };
    listOfRisks.forEach(function (element) {
        dbo.collection('risks').insert(element);
        console.log(dbo.collection('risks').find());
        //console.log(element);
    })*/
//});

//using mongodb locally
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017";
MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("autorisk");
    //var query = { address: "Park Lane 38" };
    dbo.collection('risks').remove({});
    listOfRisks.forEach(function (element) {
        dbo.collection('risks').insert(element);
        //console.log(dbo.collection('risks').find());
        //console.log(element);
    })
    /* dbo.collection("customers").find(query).toArray(function(err, result) {
       if (err) throw err;
       console.log(result);
       db.close();
     });*/
});

/*mongoose.connect("mongodb://127.0.0.1:27017/autorisk");
//mongoose.connect("mongodb://admin:password@ds263989.mlab.com:63989/autorisk");
db_mongoose.on("error", console.error.bind(console, "Connection error:"));
db_mongoose.once("open", function(callback){
     console.log("Connection Succeeded."); //once the database connection has succeeded, the code in db.once is executed.
 });*/



/*listOfRisks.forEach(risk => {
db_mongoose.col
    
});*/
/*module.exports = mongoose.model('data', new Schema({ 
    data: JSON.stringify(listOfRisks),
}));*/

//check if port environment variable is configured, else use 3000
const port = process.env.PORT || 3000;
//-------listen on port 3000 http--------
app.listen(port, () => console.log(`Listening on port ${port}...`));

/*
app.post();
app.put();
app.delete();*/